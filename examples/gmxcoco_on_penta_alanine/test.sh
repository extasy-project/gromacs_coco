#!/bin/bash

cp ../../usecases/data_penta_alanine/* .

echo "1 | 0" > index.dat
echo "q" >> index.dat

make_ndx -f helix.gro -o index.ndx < index.dat

echo "Protein" | genrestr -f helix.gro -n index.ndx -fc 10 10 10 #will produce posre.itp file in output

cp grompp-em1.mdp grompp_after_coco-em1.mdp
cp grompp-em2.mdp grompp_after_coco-em2.mdp

echo 'define=-DPOSRES' >> grompp_after_coco-em1.mdp
echo 'define=-DPOSRES' >> grompp_after_coco-em2.mdp

# Now create the configuration file for gmxcoco with the appropriate parameters - here named coco_penta.conf

gmxcoco -c coco_penta.conf --nreps 2 --maxcycles 2 -vv

# After this point we could modify and use the last line only without the need to repeat the preprocessing
# that is only a one-off procedure.
