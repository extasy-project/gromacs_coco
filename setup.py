#!/usr/bin/env python
""" Setup script. Used by easy_install and pip. """

import os
import sys
import subprocess as sp
import re

from setuptools import setup, find_packages, Command

'''Discover the package version'''
VERSIONFILE="src/gmxcoco_wkflw/_version.py"
verstrline = open(VERSIONFILE, "rt").read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, verstrline, re.M)
if mo:
    verstr = mo.group(1)
else:
    raise RuntimeError("Unable to find version string in {0}.".format(VERSIONFILE))


#-----------------------------------------------------------------------------
# check python version. we need > 2.5, <3.x
if  sys.hexversion < 0x02050000 or sys.hexversion >= 0x03000000:
    raise RuntimeError("%s requires Python 2.x (2.5 or higher)" % name)


#-----------------------------------------------------------------------------
#
def read(*rnames):
    return open(os.path.join(os.path.dirname(__file__), *rnames)).read()


#-----------------------------------------------------------------------------
setup_args = {
    'name'             : "extasy.gromacs_coco",
    'version'          : verstr,
    'description'      : "EXTASY Project - Gromacs CoCo workflow",
    'long_description' : (read('README.md') + '\n\n' + read('CHANGES.md')),
    'author'           : "The EXTASY Project",
    'url'              : "https://bitbucket.org/extasy-project/extasy-project",
    'download_url'     : "https://bitbucket.org/extasy-project/gromacs_coco/get/"+verstr+".tar.gz",
    'license'          : "T.B.D but most probably BSD2",
    'classifiers'      : [
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.5',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Topic :: Utilities',
        'Topic :: System :: Distributed Computing',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX',
        'Operating System :: Unix'
    ],

    'packages'    : find_packages('src'),
    'package_dir' : {'': 'src'},
    'scripts' : ['src/gmxcoco'],
    'install_requires' : ['numpy',
                          'scipy',
                          'extasy.coco',
    		          'MDAnalysis==0.9.0'],
    'zip_safe'         : False,
}

#-----------------------------------------------------------------------------

setup (**setup_args)
